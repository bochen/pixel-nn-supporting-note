
#include <iostream>
#include "/Users/chenboping/Desktop/atlasstyle/atlasstyle-00-04-02/AtlasStyle.C"
#include "/Users/chenboping/Desktop/atlasstyle/atlasstyle-00-04-02/AtlasLabels.C"
int makeplot1btag()
{
    gStyle->SetOptFit(0101);
    gStyle->SetOptTitle(kFALSE);
    gStyle->SetOptStat(0);
    SetAtlasStyle();
    TFile *fpos=new TFile("pos2.root");
    TH1D * residuals_all_all_X = (TH1D*)fpos->Get("residuals_all_all_X");
    TH1D * residuals_all_all_Y = (TH1D*)fpos->Get("residuals_all_all_Y");
    TH1D * residuals_b1_all_X = (TH1D*)fpos->Get("residuals_b1_all_X");
    TH1D * residuals_b1_all_Y = (TH1D*)fpos->Get("residuals_b1_all_Y");
    TH1D * residuals_b2_all_X = (TH1D*)fpos->Get("residuals_b2_all_X");
    TH1D * residuals_b2_all_Y = (TH1D*)fpos->Get("residuals_b2_all_Y");
    TH1D * residuals_b3_all_X = (TH1D*)fpos->Get("residuals_b3_all_X");
    TH1D * residuals_b3_all_Y = (TH1D*)fpos->Get("residuals_b3_all_Y");
    TH1D * residuals_barrel_all_X = (TH1D*)fpos->Get("residuals_barrel_all_X");
    TH1D * residuals_barrel_all_Y = (TH1D*)fpos->Get("residuals_barrel_all_Y");
    TH1D * residuals_endcap_all_X = (TH1D*)fpos->Get("residuals_endcap_all_X");
    TH1D * residuals_endcap_all_Y = (TH1D*)fpos->Get("residuals_endcap_all_Y");
    TH1D * residuals_ibl_all_X = (TH1D*)fpos->Get("residuals_ibl_all_X");
    TH1D * residuals_ibl_all_Y = (TH1D*)fpos->Get("residuals_ibl_all_Y");
    TH1D* residual[14]={residuals_all_all_X, residuals_all_all_Y, residuals_b1_all_X, residuals_b1_all_Y, residuals_b2_all_X, residuals_b2_all_Y, residuals_b3_all_X, residuals_b3_all_Y, residuals_barrel_all_X, residuals_barrel_all_Y, residuals_endcap_all_X, residuals_endcap_all_Y, residuals_ibl_all_X, residuals_ibl_all_Y};
    TString title[14]={"residuals_all_all_X", "residuals_all_all_Y", "residuals_b1_all_X", "residuals_b1_all_Y", "residuals_b2_all_X", "residuals_b2_all_Y", "residuals_b3_all_X", "residuals_b3_all_Y", "residuals_barrel_all_X", "residuals_barrel_all_Y", "residuals_endcap_all_X", "residuals_endcap_all_Y", "residuals_ibl_all_X", "residuals_ibl_all_Y"};
    
    for(int i=0;i<14;i++)
    {
        TCanvas *c1 = new TCanvas("c1","c1",800,600);
        residual[i]->GetXaxis()->SetTitle("Truth hit residual [mm]");
        residual[i]->GetYaxis()->SetTitle("");
        double mean, rms=0;
        mean = residual[i]->GetMean();
        rms = residual[i]->GetStdDev();
        residual[i]->GetXaxis()->SetLabelSize(0.05);
        residual[i]->DrawNormalized();
        TString smean = "mean = "+to_string(mean);
        TString srms = "rms = "+to_string(rms);
        TLatex *tex = new TLatex(0.7,0.82,smean);
        tex->SetNDC();
        tex->SetTextSize(0.03);
        tex->Draw();
        TLatex *tex1 = new TLatex(0.7,0.75,srms);
        tex1->SetNDC();
        tex1->SetTextSize(0.03);
        tex1->Draw();
        
        TString clusters = "2-particle clusters";
        TLatex *tex2 = new TLatex(0.2,0.75,clusters);
        tex2->SetNDC();
        tex2->SetTextSize(0.03);
        tex2->Draw();
        
        TString detector = "all";
        if(i==0||i==1) detector="All";
        else if (i==2||i==3) detector="b1 layer";
        else if (i==4||i==5) detector="b2 layer";
        else if (i==6||i==7) detector="b3 layer";
        else if (i==8||i==9) detector="Barrel";
        else if (i==10||i==11) detector="Endcap";
        else if (i==12||i==13) detector="IBL";
        TLatex *tex4 = new TLatex(0.2,0.7,detector);
        tex4->SetNDC();
        tex4->SetTextSize(0.03);
        tex4->Draw();

        TString direction = "xy";
        if(i%2==0) direction = "local X direction";
        else if(i%2==1) direction = "local Y direction";
        TLatex *tex5 = new TLatex(0.2,0.65,direction);
        tex5->SetNDC();
        tex5->SetTextSize(0.03);
        tex5->Draw();
        
        ATLASLabel(0.2,0.8,"Internal");
        c1->SaveAs(title[i]+".pdf");
    }


    /*comsingle->GetXaxis()->SetTitle("m_{X#gamma J} [GeV]");
    comsingle->GetYaxis()->SetTitle();
    comsingle->SetMaximum(2);
    comsingle->SetMinimum(0.05);
//    comsingle->SetMarkerColor(4);
//    comsingle->SetMarkerSize(0.5);
//    comsingle->SetMarkerStyle(21);
    comsingle->SetLineColor(2);
    comsingle->SetLineWidth(4);
//    comsingle->Draw("ACP");

    //vrsingle->SetMarkerColor(1);
    //vrsingle->SetMarkerSize(1);
    //vrsingle->SetMarkerStyle(21);
    vrsingle->SetLineColor(1);
    vrsingle->SetLineWidth(4);
//    vrsingle->Draw("SAME ACP");

    //comdouble->SetMarkerColor(6);
//    comdouble->SetMarkerSize(0.5);
//    comdouble->SetMarkerStyle(21);
    comdouble->SetLineColor(4);
    comdouble->SetLineWidth(4);
//    comdouble->Draw("SAME ACP");

//    vrdouble->SetMarkerColor(1);
//    vrdouble->SetMarkerSize(0.5);
//    vrdouble->SetMarkerStyle(21);
//    vrdouble->SetLineColor(6);
//    vrdouble->SetLineWidth(4);
//    vrdouble->Draw("SAME ACP");

    TMultiGraph *mg = new TMultiGraph();
    mg->Add(comsingle);
    mg->Add(vrsingle);
    mg->Add(comdouble);
//    mg->Add(vrdouble);
    mg->Draw("AC");
    mg->SetMaximum(100);
    mg->SetMinimum(0.01);
    mg->GetYaxis()->SetTitle("1btag Expected cross section limit[fb]");
    mg->GetYaxis()->SetTitleSize(0.06);
    mg->GetYaxis()->SetTitleOffset(1.2);
    
    

    TLegend leg(.6,.6,.9,.9,"");
    leg.SetBorderSize(0);
    leg.SetFillColor(0);
    leg.SetTextSize(0.04);
    leg.SetTextFont(42);
    leg.AddEntry(&(*comsingle),"Only stat.","l");
    leg.AddEntry(&(*vrsingle),"With spurious signal","l");
//    leg.AddEntry(&(*vrdouble),"VR 85%");
    leg.AddEntry(&(*comdouble),"With full systematic","l");
    leg.DrawClone("Same");

    TLatex *tex = new TLatex(0.2,0.7,"#int Ldt=139fb^{-1},#sqrt{s}= 13 TeV");
    tex->SetNDC();
    tex->SetTextAlign(12);
    tex->SetTextSize(0.05);
    tex->SetLineWidth(1);
    tex->Draw();
    TLatex *tex1 = new TLatex(0.2,0.6,"1btag category");
    tex1->SetNDC();
    tex1->SetTextAlign(12);
    tex1->SetTextSize(0.05);
    tex1->SetLineWidth(1);
    tex1->Draw();
    ATLASLabel(0.2,0.8,"Internal");
    
    c1->cd();
    TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.35);
    pad2->SetTopMargin(0);
    pad2->SetBottomMargin(0.3);
    pad2->Draw();
    pad2->cd();
    pad2->SetGrid();

//    comcombine->SetMarkerColor(9);
//    comcombine->SetMarkerSize(0.5);
//    comcombine->SetMarkerStyle(21);
    comcombine->SetLineColor(1);
    comcombine->SetLineWidth(4);
    comcombine->SetMaximum(1.1);
    comcombine->SetMinimum(0.7);
    comcombine->GetXaxis()->SetTitle("m_{#gamma J} [GeV]");
    comcombine->GetXaxis()->SetTitleSize(0.05);
    comcombine->GetXaxis()->SetLabelSize(0.05);
//    comcombine->Draw("SAME AC");

//    vrcombine->SetMarkerColor(7);
//    vrcombine->SetMarkerSize(0.5);
//    vrcombine->SetMarkerStyle(21);
//    vrcombine->SetLineColor(4);
//    vrcombine->GetXaxis()->SetTitle("m_{X} [GeV]");
//    vrcombine->SetLineWidth(4);
    
    ratio7785->SetLineColor(2);
    ratio7785->SetLineWidth(4);
//    vrcombine->Draw("SAME AC");

    TMultiGraph *mg1 = new TMultiGraph();
    mg1->Add(comcombine);
//    mg1->Add(vrcombine);
    mg1->Add(ratio7785);
    mg1->Draw("AC");
    mg1->GetXaxis()->SetTitle("m_{#gamma J} [GeV]");
    mg1->GetXaxis()->SetTitleSize(0.1);
    mg1->GetXaxis()->SetLabelSize(0.1);
    mg1->GetYaxis()->SetLabelSize(0.07);
    mg1->GetYaxis()->SetTitleSize(0.1);
    mg1->GetYaxis()->SetTitleOffset(0.5);
    mg1->GetYaxis()->SetTitle("Ratio to full sys.");
    mg1->SetMaximum(1.1);
    mg1->SetMinimum(0.8);

    TLegend leg1(.7,.7,.9,.9,"Limit");
    leg1.SetBorderSize(0);
    leg1.SetFillColor(0);
    leg1.SetTextSize(0.025);
    //leg1.AddEntry(&(*comcombine),"signal fitting","lep");
//    leg1.AddEntry(&(*vrcombine),"85% CoM/VR");
    //leg1.AddEntry(&(*ratio7785),"with par system","lep");

    //leg1.DrawClone("Same");
    
*/

    return 0;
}




